<?php $__env->startSection('page-title',__('site.application-complete')); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->startSection('breadcrumb'); ?>

                    <li class="breadcrumb-item"><a href="<?php echo e(route('vacancies')); ?>"><?php echo app('translator')->get('site.vacancies'); ?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo app('translator')->get('site.application-complete'); ?></li>


<?php $__env->stopSection(); ?>
<p>
<?php echo app('translator')->get('site.application-complete-msg'); ?>
</p>
<?php $__env->stopSection(); ?>

<?php echo $__env->make($userLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/candidate/vacancies/complete.blade.php ENDPATH**/ ?>