<?php $__env->startSection('search-form',route('admin.attachments.index',['user'=>$user->id])); ?>

<?php $__env->startSection('pageTitle',__('site.attachments')); ?>
<?php $__env->startSection('page-title',__('site.attachments').': '.$user->name." ({$type})".(!empty(request('search')) ? ': '.request('search'):'')); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div >
                    <div  >
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_candidate_attachments')): ?>
                        <a href="<?php echo e($route); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','create_candidate_attachment')): ?>
                        <a href="<?php echo e(route('admin.attachments.create',['user'=>$user->id])); ?>" class="btn btn-success btn-sm" title="<?php echo app('translator')->get('site.add-new'); ?>">
                            <i class="fa fa-plus" aria-hidden="true"></i> <?php echo app('translator')->get('site.add-new'); ?>
                        </a>
                        <?php endif; ?>




                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th><?php echo app('translator')->get('site.name'); ?></th>
                                        <th><?php echo app('translator')->get('site.type'); ?></th>
                                        <th><?php echo app('translator')->get('site.added-on'); ?></th>
                                        <th><?php echo app('translator')->get('site.author'); ?></th>
                                        <th><?php echo app('translator')->get('site.actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration + ( (Request::get('page',1)-1) * $perPage)); ?></td>
                                        <td><?php echo e($item->name); ?></td>
                                        <td>
                                            <?php echo e(pathinfo($item->path,PATHINFO_EXTENSION)); ?>

                                        </td>
                                        <td><?php echo e(\Illuminate\Support\Carbon::parse($item->created_at)->format('d/M/Y')); ?></td>
                                        <td><?php echo e($item->author); ?></td>
                                        <td>
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_candidate_attachment')): ?>
                                            <?php if(isImage($item->path)): ?>
                                             <a href="#"  data-toggle="modal" data-target="#pictureModal<?php echo e($item->id); ?>"  title="<?php echo app('translator')->get('site.view'); ?>"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo app('translator')->get('site.view'); ?></button></a>
                                                <div class="modal fade" id="pictureModal<?php echo e($item->id); ?>" tabindex="-1" role="dialog" aria-labelledby="pictureModal<?php echo e($item->id); ?>Label" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="pictureModal<?php echo e($item->id); ?>Label"><?php echo app('translator')->get('site.picture'); ?></h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body int_txcen" >
                                                                <img src="<?php echo e(route('admin.image')); ?>?file=<?php echo e($item->path); ?>" class="int_wm100pc"  />
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo app('translator')->get('site.close'); ?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                            <?php endif; ?>
                                            <a href="<?php echo e(route('admin.download')); ?>?file=<?php echo e($item->path); ?>" title="<?php echo app('translator')->get('site.download'); ?>"><button class="btn btn-success btn-sm"><i class="fa fa-download" aria-hidden="true"></i> <?php echo app('translator')->get('site.download'); ?></button></a>
                                            <?php endif; ?>

                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','edit_candidate_attachment')): ?>
                                            <a href="<?php echo e(url('/admin/attachments/' . $item->id . '/edit')); ?>" title="<?php echo app('translator')->get('site.edit'); ?>"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> <?php echo app('translator')->get('site.edit'); ?></button></a>
                                            <?php endif; ?>

                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','delete_candidate_attachment')): ?>
                                            <form method="POST" action="<?php echo e(url('/admin/attachments' . '/' . $item->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <button type="submit" class="btn btn-danger btn-sm" title="<?php echo app('translator')->get('site.delete'); ?>" onclick="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo app('translator')->get('site.delete'); ?></button>
                                            </form>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo clean( $attachments->appends(['search' => Request::get('search')])->render() ); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page-wide', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/attachments/index.blade.php ENDPATH**/ ?>