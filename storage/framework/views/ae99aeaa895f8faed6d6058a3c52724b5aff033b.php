<?php $__env->startSection('search-form',route('admin.applications.index',['vacancy'=>$vacancy->id])); ?>

<?php $__env->startSection('pageTitle',__('site.applications').': '.$vacancy->title); ?>
<?php $__env->startSection('page-title',__('site.applications').' ('.$applications->count().')'.': '.$vacancy->title); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div >
                    <div>
                        <?php if(!empty($filterParams)): ?>
                            <ul  class="list-inline">
                                <li class="list-inline-item" ><strong><?php echo app('translator')->get('site.filter'); ?>:</strong></li>
                                <?php $__currentLoopData = $filterParams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $param): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(null !== request()->get($param)  && request()->get($param) != ''): ?>
                                        <li class="list-inline-item" ><?php echo e(ucwords(str_ireplace('_',' ',$param))); ?></li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </ul>
                        <?php endif; ?>
                    </div>
                    <div  >

                        <a href="<?php echo e(url('/admin/vacancies')); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>


                        <a data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="filterCollapse" class="btn btn-primary btn-sm" title="<?php echo app('translator')->get('site.filter'); ?>">
                            <i class="fa fa-filter" aria-hidden="true"></i> <?php echo app('translator')->get('site.filter'); ?>
                        </a>

                        <a  href="<?php echo e(route('admin.applications.index',['vacancy'=>$vacancy->id])); ?>" class="btn btn-info btn-sm" title="<?php echo app('translator')->get('site.reset'); ?>">
                            <i class="fa fa-sync" aria-hidden="true"></i> <?php echo app('translator')->get('site.reset'); ?>
                        </a>

                        <div class="collapse int_tm20" id="filterCollapse"  >
                            <div  >
                                <form action="<?php echo e(route('admin.applications.index',['vacancy'=>$vacancy->id])); ?>" method="get">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="search" class="control-label"><?php echo app('translator')->get('site.search'); ?></label>
                                                <input class="form-control" type="text" value="<?php echo e(request()->search); ?>" name="search"/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2 <?php echo e($errors->has('gender') ? 'has-error' : ''); ?>">
                                            <label for="gender" class="control-label"><?php echo app('translator')->get('site.gender'); ?></label>
                                            <select name="gender" class="form-control" id="gender" >
                                                <option></option>
                                                <?php $__currentLoopData = json_decode('{"f":"'.__('site.female').'","m":"'.__('site.male').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('gender',@request()->gender)) && old('gender',@request()->gender) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <?php echo clean( $errors->first('gender', '<p class="help-block">:message</p>') ); ?>

                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="search" class="control-label"><?php echo app('translator')->get('site.min-age'); ?></label>
                                            <select class="form-control" name="min_age" id="min_age">
                                                <option></option>
                                                <?php $__currentLoopData = range(1,100); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if(request()->min_age==$value): ?> selected  <?php endif; ?> value="<?php echo e($value); ?>"><?php echo e($value); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="search" class="control-label"><?php echo app('translator')->get('site.max-age'); ?></label>
                                            <select class="form-control" name="max_age" id="max_age">
                                                <option></option>
                                                <?php $__currentLoopData = range(1,100); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if(request()->max_age==$value): ?> selected  <?php endif; ?> value="<?php echo e($value); ?>"><?php echo e($value); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="field_id" class="control-label"><?php echo app('translator')->get('site.custom-field'); ?></label>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <select class="form-control" name="field_id" id="field_id">
                                                        <option ></option>
                                                        <?php $__currentLoopData = \App\CandidateField::orderBy('sort_order')->where('type','!=','file')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($field->id==request()->get('field_id')): ?> selected <?php endif; ?> value="<?php echo e($field->id); ?>"><?php echo e($field->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    =
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control" type="text" name="custom_field" value="<?php echo e(request()->get('custom_field')); ?>"/>
                                                </div>
                                            </div>

                                        </div>





                                        <div class="form-group  col-md-2<?php echo e($errors->has('shortlisted') ? 'has-error' : ''); ?>">
                                            <label for="shortlisted" class="control-label"><?php echo app('translator')->get('site.shortlisted'); ?></label>
                                            <select name="shortlisted" class="form-control" id="shortlisted" >
                                                <option></option>
                                                <?php $__currentLoopData = json_decode('{"0":"'.__('site.no').'","1":"'.__('site.yes').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('shortlisted',@request()->shortlisted)) && old('shortlisted',@request()->shortlisted) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <?php echo clean( $errors->first('shortlisted', '<p class="help-block">:message</p>') ); ?>

                                        </div>


                                        <div class="col-md-2">

                                            <div class="form-group">
                                                <label for="min_date" class="control-label"><?php echo app('translator')->get('site.min-date'); ?></label>
                                                <input class="form-control date" type="text" value="<?php echo e(request()->min_date); ?>" name="min_date"/>
                                            </div>

                                        </div>
                                        <div class="col-md-2">

                                            <div class="form-group">
                                                <label for="max_date" class="control-label"><?php echo app('translator')->get('site.max-date'); ?></label>
                                                <input class="form-control date" type="text" value="<?php echo e(request()->max_date); ?>" name="max_date"/>
                                            </div>

                                        </div>




                                    </div>

                                    <div>
                                        <button type="submit" class="btn btn-primary btn-block"><?php echo app('translator')->get('site.filter'); ?></button>
                                    </div>

                                </form>
                            </div>
                        </div>




                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo app('translator')->get('site.picture'); ?></th>
                                        <th><?php echo app('translator')->get('site.candidate'); ?></th>
                                        <th><?php echo app('translator')->get('site.gender'); ?></th>
                                        <th><?php echo app('translator')->get('site.email'); ?></th>
                                        <th><?php echo app('translator')->get('site.age'); ?></th>
                                        <th><?php echo app('translator')->get('site.shortlisted'); ?></th>
                                        <th><?php echo app('translator')->get('site.added-on'); ?></th>
                                        <th><?php echo app('translator')->get('site.actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration + ( (Request::get('page',1)-1) *$perPage)); ?></td>
                                        <td><div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                  <?php if(!empty($item->user->candidate->picture) && file_exists($item->user->candidate->picture)): ?>
                      <img   src="<?php echo e(asset($item->user->candidate->picture)); ?>">
                  <?php else: ?>
                      <img   src="<?php echo e(asset('img/man.jpg')); ?>">
                  <?php endif; ?>

              </span>
                                            </div></td>
                                        <td><?php echo e($item->user->name); ?></td>
                                        <td><?php echo e(gender($item->user->candidate->gender)); ?></td>
                                        <td><?php echo e($item->user->email); ?></td>
                                        <td><?php echo e(getAge(\Illuminate\Support\Carbon::parse($item->user->candidate->date_of_birth)->timestamp)); ?>

                                            </td>
                                        <td>
                                            <?php echo e(boolToString($item->shortlisted)); ?>

                                        </td>
                                        <td><?php echo e(\Illuminate\Support\Carbon::parse($item->created_at)->format('d/M/Y')); ?></td>

                                        <td>

                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ni ni-settings"></i> <?php echo app('translator')->get('site.actions'); ?>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_application')): ?>
                                                    <a class="dropdown-item" href="<?php echo e(route('admin.download')); ?>?file=<?php echo e($item->user->candidate->cv_path); ?>"><?php echo app('translator')->get('site.download-cv'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_candidate')): ?>
                                                    <a target="_blank" class="dropdown-item" href="<?php echo e(route('admin.candidates.show',['candidate'=>$item->user_id])); ?>"><?php echo app('translator')->get('site.view-candidate'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','shortlist_application')): ?>
                                                    <a class="dropdown-item" href="<?php echo e(route('admin.applications.shortlist',['application'=>$item->id])); ?>?status=<?php echo e($item->shortlisted==1? 0:1); ?>"><?php echo e($item->shortlisted==1? __('site.remove-from').' ':''); ?><?php echo app('translator')->get('site.shortlist'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','delete_application')): ?>
                                                    <a class="dropdown-item" href="#" onclick="$('#deleteForm<?php echo e($item->id); ?>').submit()"><?php echo app('translator')->get('site.delete'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_test_results')): ?>
                                                    <a target="_blank" class="dropdown-item" href="<?php echo e(route('admin.candidates.tests',['user'=>$item->user_id])); ?>" ><?php echo app('translator')->get('site.test-results'); ?> (<?php echo e($item->user->userTests()->count()); ?>)</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            <form  onsubmit="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>&quot;)"   id="deleteForm<?php echo e($item->id); ?>"  method="POST" action="<?php echo e(url('/admin/applications' . '/' . $item->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo clean( $applications->appends(request()->input())->render() ); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(asset('vendor/pickadate/picker.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('vendor/pickadate/picker.date.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('vendor/pickadate/picker.time.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('vendor/pickadate/legacy.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/order-search.js')); ?>"></script>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('header'); ?>
    ##parent-placeholder-594fd1615a341c77829e83ed988f137e1ba96231##
    <link href="<?php echo e(asset('vendor/pickadate/themes/default.date.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('vendor/pickadate/themes/default.time.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('vendor/pickadate/themes/default.css')); ?>" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page-wide', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/applications/index.blade.php ENDPATH**/ ?>