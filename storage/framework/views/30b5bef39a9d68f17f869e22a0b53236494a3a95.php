<?php $__env->startSection('page-title',__('site.vacancy').' : '.$vacancy->title); ?>
<?php $__env->startSection('inline-title',__('site.vacancy').' : '.$vacancy->title); ?>
<?php $__env->startSection('crumb'); ?>
    <li><a href="<?php echo e(route('vacancies')); ?>"><?php echo app('translator')->get('site.vacancies'); ?></a></li>
    <li><?php echo app('translator')->get('site.vacancy-details'); ?></li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <!-- Start Job Details -->
    <div class="job-details section">
        <div class="container">
            <?php echo $__env->make('partials.flash_message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="row mb-n5">
                <!-- Job List Details Start -->
                <div class="col-lg-8 col-12">
                    <div class="job-details-inner">
                        <div class="job-details-head row mx-0">

                            <div class="salary-type col-auto order-sm-3">
                                <span class="salary-range"><?php echo e($vacancy->salary); ?></span>
                                <span class="badge badge-success"><?php echo e(\Illuminate\Support\Carbon::parse($vacancy->updated_at)->diffForHumans()); ?></span>
                            </div>
                            <div class="content col">
                                <h5 class="title"><?php echo e($vacancy->title); ?></h5>
                                <ul class="meta">

                                    <li><i class="lni lni-map-marker"></i> <?php echo e($vacancy->location); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="job-details-body">
                            <h6 class="mb-3"><?php echo e(__t('job-description')); ?></h6>
                            <p><?php echo clean($vacancy->description); ?></p>

                        </div>
                    </div>
                </div>
                <!-- Job List Details End -->
                <!-- Job Sidebar Wrap Start -->
                <div class="col-lg-4 col-12">
                    <div class="job-details-sidebar">
                        <!-- Sidebar (Apply Buttons) Start -->
                        <div class="sidebar-widget">
                            <div class="inner">
                                <div class="row m-n2 button">
                                        <a  onclick="return confirm('<?php echo app('translator')->get('site.confirm-application'); ?>')"  href="<?php echo e(route('candidate.vacancy.apply',['vacancy'=>$vacancy->id])); ?>" class="d-block btn btn-primary"><?php echo app('translator')->get('site.apply'); ?></a>

                                </div>
                            </div>
                        </div>
                        <!-- Sidebar (Apply Buttons) End -->
                        <!-- Sidebar (Job Overview) Start -->
                        <div class="sidebar-widget">
                            <div class="inner">
                                <h6 class="title"><?php echo e(__t('job-overview')); ?></h6>
                                <ul class="job-overview list-unstyled">
                                    <li><strong><?php echo e(__t('published-on')); ?>:</strong> <?php echo e($vacancy->updated_at->format('M d, Y')); ?></li>

                                    <li><strong><?php echo e(__t('job-location')); ?>:</strong> <?php echo e($vacancy->location); ?></li>
                                    <li><strong><?php echo app('translator')->get('site.salary'); ?>:</strong> <?php echo e($vacancy->salary); ?></li>
                                    <?php if(!empty($vacancy->closes_at)): ?>
                                    <li><strong><?php echo e(__t('application-deadline')); ?>:</strong> <?php echo e(\Illuminate\Support\Carbon::parse($vacancy->closes_at)->format('M d, Y')); ?></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- Sidebar (Job Overview) End -->

                        <?php if(!empty($vacancy->location)): ?>
                        <!-- Sidebar (Job Location) Start -->
                        <div class="sidebar-widget">
                            <div class="inner">
                                <h6 class="title"><?php echo e(__t('job-location')); ?></h6>
                                <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo e(urlencode($vacancy->location)); ?>&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style></div></div>
                            </div>
                        </div>
                        <!-- Sidebar (Job Location) End -->
                        <?php endif; ?>
                    </div>
                </div>
                <!-- Job Sidebar Wrap End -->

            </div>
        </div>
    </div>
    <!-- End Job Details -->



<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make($templateLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\public\templates/jobportal/views/candidate/vacancies/view.blade.php ENDPATH**/ ?>