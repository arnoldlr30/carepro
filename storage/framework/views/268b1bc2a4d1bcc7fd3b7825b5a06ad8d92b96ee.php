<?php $__env->startSection('page-title',__('site.order-complete')); ?>

<?php $__env->startSection('content'); ?>
        <p>
            <?php echo app('translator')->get('site.order-complete-text'); ?>
        </p>

<?php $__env->stopSection(); ?>

<?php echo $__env->make($templateLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/employer/order/complete.blade.php ENDPATH**/ ?>