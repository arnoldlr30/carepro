<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e(__('site.name')); ?></label>
    <input required class="form-control" name="name" type="text" id="name" value="<?php echo e(old('name',isset($contract->name) ? $contract->name : '')); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group  <?php echo e($errors->has('users') ? 'has-error' : ''); ?>">

    <label for="users"><?php echo app('translator')->get('site.signatories'); ?></label>

        <select required multiple name="users[]" id="users" class="form-control select2">
            <option></option>
        </select>


    <?php echo clean( $errors->first('users', '<p class="help-block">:message</p>') ); ?>

</div>

<div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
    <label for="description" class="control-label"><?php echo e(__('site.description')); ?> (<?php echo app('translator')->get('site.optional'); ?>)</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" ><?php echo e(old('description',isset($contract->description) ? $contract->description : '')); ?></textarea>
    <?php echo $errors->first('description', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('enabled') ? 'has-error' : ''); ?>">
    <label for="enabled" class="control-label"><?php echo e(__('site.enabled')); ?></label>
    <select name="enabled" class="form-control" id="enabled" >
    <?php $__currentLoopData = json_decode('{"0":"'.__('site.no').'","1":"'.__('site.yes').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('enabled',@$contract->enabled)) && old('enabled',@$contract->enabled) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
    <?php echo $errors->first('enabled', '<p class="help-block">:message</p>'); ?>

</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? __('site.update') : __('site.create')); ?>">
</div>
<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(asset('vendor/select2/js/select2.min.js')); ?>"></script>
    <script  type="text/javascript">
        "use strict";
        $('#users').select2({
            placeholder: "<?php echo app('translator')->get('site.search-users'); ?>...",
            minimumInputLength: 3,
            ajax: {
                url: '<?php echo e(route('admin.users.search')); ?>',
                dataType: 'json',
                data: function (params) {
                    return {
                        term: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }

        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('vendor/select2/css/select2.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/contracts/form.blade.php ENDPATH**/ ?>