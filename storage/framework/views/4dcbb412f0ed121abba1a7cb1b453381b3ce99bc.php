<?php $__env->startSection('pageTitle',__('site.edit-contract')); ?>
<?php $__env->startSection('page-title',$contract->name); ?>

<?php $__env->startSection('page-content'); ?>
    <?php if($contract->enabled==0): ?>
        <p class="alert alert-warning"><?php echo app('translator')->get('site.disabled-contract'); ?><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    <?php endif; ?>
    <form method="POST" action="<?php echo e(url('/admin/contracts/' . $contract->id)); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        <?php echo e(method_field('PATCH')); ?>

        <?php echo e(csrf_field()); ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="<?php echo e(url('/admin/contracts')); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button type="button" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                            </div>
                            <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary float-right">
                                        <i class="fa fa-save"></i> <?php echo e(__('site.save')); ?>

                                    </button>
                            </div>
                        </div>

                        <br />

                            <ul class="nav nav-pills" id="myTab3" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-book"></i> <?php echo e(__('site.contract')); ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-cogs"></i> <?php echo app('translator')->get('site.settings'); ?></a>
                                </li>

                            </ul>
                            <div class="tab-content" id="myTabContent2">
                                <div class="tab-pane fade show active pt-3 " id="home3" role="tabpanel" aria-labelledby="home-tab3">

                                    <div class="row pb-1">
                                        <div class="col-md-4">
                                            <span id="template-loader"></span>
                                            <select class="form-control select2" name="template" id="template" placeholder="<?php echo app('translator')->get('site.load-template'); ?>">
                                                <option>--<?php echo app('translator')->get('site.load-template'); ?>--</option>
                                                <?php $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($template->id); ?>"><?php echo e($template->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="dropdown">
                                                <button class="btn btn-block btn-primary dropdown-toggle" type="button" id="signatorydropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <?php echo app('translator')->get('site.signatory-placeholders'); ?>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                    <a class="dropdown-item"><?php echo e(__('site.signature')); ?></a>

                                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="dropdown-item placeholder-link" data-code="[<?php echo e('signature-'.$user->id); ?>]" href="javascript:;" ><?php echo e($user->name); ?> (<?php echo e(roleName($user->role_id)); ?>) - <strong>[<?php echo e('signature-'.$user->id); ?>]</strong></a>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item"><?php echo e(__('site.name')); ?></a>

                                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <a class="dropdown-item placeholder-link" data-code="[<?php echo e('name-'.$user->id); ?>]" href="javascript:;" ><?php echo e($user->name); ?> (<?php echo e(roleName($user->role_id)); ?>) - <strong>[<?php echo e('name-'.$user->id); ?>]</strong></a>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item"><?php echo e(__('site.sign-date')); ?></a>

                                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <a class="dropdown-item placeholder-link" data-code="[<?php echo e('date-'.$user->id); ?>]" href="javascript:;" ><?php echo e($user->name); ?> (<?php echo e(roleName($user->role_id)); ?>) - <strong>[<?php echo e('date-'.$user->id); ?>]</strong></a>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                               <i class="fa fa-signature"></i>  <?php echo app('translator')->get('site.add-signature'); ?>
                                            </button>
                                            <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title"><?php echo app('translator')->get('site.sign-here'); ?></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                              <div id="signatureBox" style="border: dotted 1px #CCCCCC;">
                                                               </div>
                                                          </div>
                                                          <div class="modal-footer bg-whitesmoke br">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo app('translator')->get('site.close'); ?></button>
                                                            <button type="button" class="btn btn-primary" id="signatureBtn"><?php echo app('translator')->get('site.insert'); ?></button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                        </div>
                                    </div>

                                    <div class="form-group <?php echo e($errors->has('content') ? 'has-error' : ''); ?>">
                                        <textarea class="form-control" rows="5" name="content" type="textarea" id="content" ><?php echo e(old('content',$contract->content)); ?></textarea>
                                        <?php echo $errors->first('content', '<p class="help-block">:message</p>'); ?>

                                    </div>



                                </div>
                                <div class="tab-pane fade  tab-padding" id="profile3" role="tabpanel" aria-labelledby="profile-tab3">

                                 <div class="pr-4">
                                    <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                                        <label for="name" class="control-label"><?php echo e(__('site.name')); ?></label>
                                        <input required class="form-control" name="name" type="text" id="name" value="<?php echo e(old('name',isset($contract->name) ? $contract->name : '')); ?>" >
                                        <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

                                    </div>
                                    <div class="form-group  <?php echo e($errors->has('users') ? 'has-error' : ''); ?>">

                                        <label for="users"><?php echo app('translator')->get('site.signatories'); ?></label>

                                        <select required multiple name="users[]" id="users" class="form-control select2">
                                            <option></option>
                                            <?php $__currentLoopData = $contract->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option  <?php if( (is_array(old('users')) && in_array(@$user->id,old('users')))  || (null === old('users'))): ?>
                                                         selected
                                                         <?php endif; ?>
                                                         value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?> (<?php echo e(roleName($user->role_id)); ?>) &lt;<?php echo e($user->email); ?>&gt;</option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <small>
                                            <?php echo app('translator')->get('site.certificate-hint'); ?>
                                        </small>

                                        <?php echo clean( $errors->first('users', '<p class="help-block">:message</p>') ); ?>

                                    </div>
                                    <div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
                                        <label for="description" class="control-label"><?php echo e(__('site.description')); ?> (<?php echo app('translator')->get('site.optional'); ?>)</label>
                                        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" ><?php echo e(old('description',isset($contract->description) ? $contract->description : '')); ?></textarea>
                                        <?php echo $errors->first('description', '<p class="help-block">:message</p>'); ?>

                                    </div>
                                    <div class="form-group <?php echo e($errors->has('enabled') ? 'has-error' : ''); ?>">
                                        <label for="enabled" class="control-label"><?php echo e(__('site.enabled')); ?></label>
                                        <select name="enabled" class="form-control" id="enabled" >
                                            <?php $__currentLoopData = json_decode('{"0":"'.__('site.no').'","1":"'.__('site.yes').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('enabled',@$contract->enabled)) && old('enabled',@$contract->enabled) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <?php echo $errors->first('enabled', '<p class="help-block">:message</p>'); ?>

                                    </div>
                                </div>

                                </div>

                            </div>





                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(asset('vendor/select2/js/select2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/summernote/summernote-bs4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/js/admin/contract-template.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jsignature/libs/jSignature.min.js')); ?>"></script>
    <script  type="text/javascript">
        "use strict";

        $('#exampleModal').on('show.bs.modal', function (e) {
            var sigdiv =  $("#signatureBox").jSignature({color:"#000000",lineWidth:5,'UndoButton':true});
            $("#signatureBox").resize();
            $('#signatureBtn').click(function(){
                $('#exampleModal').modal('hide');
                let datapair = sigdiv.jSignature("getData", "svgbase64");
                let src = "data:" + datapair[0] + "," + datapair[1];
                let code = '<img style="max-height: 100px;max-width: 100px" src="'+src+'" />';
                $('textarea#content').summernote('editor.saveRange');
                $('textarea#content').summernote('editor.restoreRange');
                $('textarea#content').summernote('editor.focus');
                $('textarea#content').summernote('editor.pasteHTML',code);
            });
        });




        $('#users').select2({
            placeholder: "<?php echo app('translator')->get('site.search-users'); ?>...",
            minimumInputLength: 3,
            ajax: {
                url: '<?php echo e(route('admin.users.search')); ?>',
                dataType: 'json',
                data: function (params) {
                    return {
                        term: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }

        });
        $('#template').select2();
        $('#template').change(function (){
            let id = $(this).val();
            if(!(id > 0)){
                return;
            }
            let status = confirm('<?php echo e(addslashes(__('site.template-warning'))); ?>');
            if(status == false){
                $('#template').val(null).trigger('change');
                return;
            }
            let url = '<?php echo e(url('admin/contracts/get-template')); ?>/'+id;
            let loader = '<img src="<?php echo e(asset('img/loader.gif')); ?>" />';
            $('#template-loader').html(loader);
            $.get(url,function(data){
                $('#template-loader').html('');
                $('textarea#content').summernote('code',data.content,{
                    height:500,
                    focus: true
                });
            });
        });
        $('.placeholder-link').click(function(){
           let code = $(this).attr('data-code');
           console.log(code);
            $('textarea#content').summernote('editor.saveRange');
            $('textarea#content').summernote('editor.restoreRange');
            $('textarea#content').summernote('editor.focus');
            $('textarea#content').summernote('editor.insertText',code);
        });
    </script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('vendor/select2/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('vendor/summernote/summernote-bs4.css')); ?>">
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/contracts/edit.blade.php ENDPATH**/ ?>