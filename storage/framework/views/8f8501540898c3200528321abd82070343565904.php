<?php $__env->startSection('page-title',__('site.apply').': '.$vacancy->title); ?>

<?php $__env->startSection('content'); ?>


    <?php $__env->startSection('breadcrumb'); ?>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('vacancies')); ?>"><?php echo app('translator')->get('site.vacancies'); ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(route('view-vacancy',['vacancy'=>$vacancy->id])); ?>"><?php echo app('translator')->get('site.vacancy-details'); ?></a></li>

                        <li class="breadcrumb-item active" aria-current="page"><?php echo app('translator')->get('site.apply'); ?></li>


    <?php $__env->stopSection(); ?>

<div class="card">
    <div class="card-body">
        <h5 class="card-title"><?php echo app('translator')->get('site.upload-cv'); ?></h5>

        <p class="card-text"><?php echo app('translator')->get('site.upload-cv-text'); ?></p>

        <form action="<?php echo e(route('candidate.vacancy.submit',['vacancy'=>$vacancy->id])); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <label for="cv"><?php echo app('translator')->get('site.cv-resume'); ?></label>
                <input name="cv" class="form-control" type="file" required />
            </div>
            <button type="submit" class="btn btn-primary  btn-block rounded"><?php echo app('translator')->get('site.apply-now'); ?></button>
        </form>


     </div>
</div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make($userLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/candidate/vacancies/apply.blade.php ENDPATH**/ ?>