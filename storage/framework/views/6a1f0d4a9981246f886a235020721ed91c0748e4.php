<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo app('translator')->get('site.name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" value="<?php echo e(old('name',isset($orderfieldgroup->name) ? $orderfieldgroup->name : '')); ?>" >
    <?php echo clean( $errors->first('name', '<p class="help-block">:message</p>') ); ?>

</div>



<div class="form-group <?php echo e($errors->has('sort_order') ? 'has-error' : ''); ?>">
    <label for="sort_order" class="control-label"><?php echo app('translator')->get('site.sort-order'); ?></label>
    <input class="form-control number" name="sort_order" type="text" id="sort_order" value="<?php echo e(old('sort_order',isset($orderfieldgroup->sort_order) ? $orderfieldgroup->sort_order : '')); ?>" >
    <?php echo clean( $errors->first('sort_order', '<p class="help-block">:message</p>') ); ?>

</div>

<div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
    <label for="description" class="control-label"><?php echo app('translator')->get('site.description'); ?></label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" ><?php echo e(old('description',isset($orderfieldgroup->description) ? $orderfieldgroup->description : '')); ?></textarea>
    <?php echo clean( $errors->first('description', '<p class="help-block">:message</p>') ); ?>

</div>

<div class="form-group <?php echo e($errors->has('layout') ? 'has-error' : ''); ?>">
    <label for="layout" class="control-label"><?php echo app('translator')->get('site.layout'); ?></label>
    <select name="layout" class="form-control" id="layout" >
        <?php $__currentLoopData = json_decode('{"v":"'.__('site.vertical').'","h":"'.__('site.horizontal').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('layout',@$orderfieldgroup->layout)) && old('layout',@$orderfieldgroup->layout) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <?php echo clean( $errors->first('layout', '<p class="help-block">:message</p>') ); ?>

</div>


<div id="column-container" class="form-group <?php echo e($errors->has('columns') ? 'has-error' : ''); ?>">
    <label for="columns" class="control-label"><?php echo app('translator')->get('site.column-width'); ?></label>

    <select name="columns" class="form-control" id="columns" >
        <?php $__currentLoopData = range(1,12); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($optionValue); ?>" <?php echo e(((null !== old('columns',@$orderfieldgroup->columns)) && old('columns',@$orderfieldgroup->columns) == $optionValue) ? 'selected' : ''); ?>><?php echo e($optionValue); ?>/12</option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <?php echo clean( $errors->first('columns', '<p class="help-block">:message</p>') ); ?>

</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? __('site.update') : __('site.create')); ?>">
</div>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/order-field-groups/form.blade.php ENDPATH**/ ?>