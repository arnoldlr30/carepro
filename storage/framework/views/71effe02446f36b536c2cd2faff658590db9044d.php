<?php $__env->startSection('pageTitle',__('Customers')); ?>
<?php $__env->startSection('page-title',__('site.profile').': '.$employer->name); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div  >
                    <div  >
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_employers')): ?>
                        <a href="<?php echo e(url('/admin/employers')); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','edit_employer')): ?>
                        <a href="<?php echo e(url('/admin/employers/' . $employer->id . '/edit')); ?>" title="<?php echo app('translator')->get('site.edit'); ?>"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.edit'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','delete_employer')): ?>
                        <form method="POST" action="<?php echo e(url('admin/employers' . '/' . $employer->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn btn-danger btn-sm" title="<?php echo app('translator')->get('site.delete'); ?>" onclick="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.delete'); ?></button>
                        </form>
                        <?php endif; ?>

                        <br/>
                        <br/>

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <?php echo app('translator')->get('site.general-details'); ?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">

                                        <div class="row">
                                        <div class=" col-md-6 <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                                            <label for="name" class="control-label"><?php echo app('translator')->get('site.name'); ?></label>
                                           <div><?php echo e($employer->name); ?></div>
                                        </div>
                                           
                                    </div>

                                        <div class="row">
                                        <div class="col-md-6 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                            <label for="email" class="control-label"><?php echo app('translator')->get('site.email'); ?></label>
                                            <div><?php echo e($employer->email); ?></div>
                                        </div>
                                        <div class="col-md-6 <?php echo e($errors->has('telephone') ? 'has-error' : ''); ?>">
                                            <label for="telephone" class="control-label"><?php echo app('translator')->get('site.telephone'); ?></label>
                                            <div><?php echo e($employer->telephone); ?></div>
                                        </div>
                                        </div>


                                        <div class="row">
                                       
                                            <div class="col-md-6 <?php echo e($errors->has('active') ? 'has-error' : ''); ?>">
                                                <label for="active" class="control-label"><?php echo app('translator')->get('site.active'); ?></label>
                                                <div><?php echo e(boolToString($employer->employer->active)); ?></div>
                                            </div>

                                        </div>




                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <label  class="control-label"><?php echo app('translator')->get('site.registered-on'); ?></label>
                                                            <div><?php echo e(\Illuminate\Support\Carbon::parse($employer->created_at)->format('d/M/Y')); ?></div>
                                                        </div>

                                                </div>


                                    </div>
                                </div>
                            </div>
                            <?php $__currentLoopData = \App\EmployerFieldGroup::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="card">
                                    <div class="card-header" id="heading<?php echo e($group->id); ?>">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo e($group->id); ?>" aria-expanded="false" aria-controls="collapse<?php echo e($group->id); ?>">
                                                <?php echo e($group->name); ?>

                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapse<?php echo e($group->id); ?>" class="collapse" aria-labelledby="heading<?php echo e($group->id); ?>" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="row">
                                            <?php $__currentLoopData = $group->employerFields()->orderBy('sort_order')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php


                                                    $value = ($employer->employerFields()->where('id',$field->id)->first()) ? $employer->employerFields()->where('id',$field->id)->first()->pivot->value:'';
                                            ?>

                                                <?php if($field->type=='text'): ?>
                                                    <div class="col-md-6<?php echo e($errors->has('field_'.$field->id) ? ' has-error' : ''); ?>">
                                                        <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>
                                                        <div><?php echo e($value); ?></div>
                                                    </div>
                                                <?php elseif($field->type=='select'): ?>
                                                    <div class="col-md-6<?php echo e($errors->has('field_'.$field->id) ? ' has-error' : ''); ?>">
                                                        <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>
                                                        <div><?php echo e($value); ?></div>

                                                    </div>
                                                <?php elseif($field->type=='textarea'): ?>
                                                    <div class="col-md-6<?php echo e($errors->has('field_'.$field->id) ? ' has-error' : ''); ?>">
                                                        <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>
                                                      <div><?php echo e($value); ?></div>
                                                    </div>
                                                <?php elseif($field->type=='checkbox'): ?>
                                                        <div class="col-md-6<?php echo e($errors->has('field_'.$field->id) ? ' has-error' : ''); ?>">
                                                            <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>
                                                            <div><?php echo e(boolToString($value)); ?></div>
                                                        </div>

                                                <?php elseif($field->type=='radio'): ?>
                                                        <div class="col-md-6<?php echo e($errors->has('field_'.$field->id) ? ' has-error' : ''); ?>">
                                                            <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>
                                                            <div><?php echo e($value); ?></div>

                                                        </div>
                                                <?php elseif($field->type=='file'): ?>


                                                        <div class="col-md-6">
                                                            <label for="<?php echo e('field_'.$field->id); ?>"><?php echo e($field->name); ?>:</label>


                                                            <?php if(!empty($value)): ?>
                                                                <h3><?php echo e(basename($value)); ?></h3>
                                                                <?php if(isImage($value)): ?>
                                                                    <div><img  data-toggle="modal" data-target="#pictureModal<?php echo e($field->id); ?>" src="<?php echo e(route('admin.image')); ?>?file=<?php echo e($value); ?>"  class="int_w330cur" /></div> <br/>


                                                                    <div class="modal fade" id="pictureModal<?php echo e($field->id); ?>" tabindex="-1" role="dialog" aria-labelledby="pictureModal<?php echo e($field->id); ?>Label" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title" id="pictureModal<?php echo e($field->id); ?>Label"><?php echo app('translator')->get('site.picture'); ?></h5>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body int_txcen"  >
                                                                                    <img src="<?php echo e(route('admin.image')); ?>?file=<?php echo e($value); ?>" class="int_txcen" />
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo app('translator')->get('site.close'); ?></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                <?php endif; ?>
                                                                 <a class="btn btn-success" href="<?php echo e(route('admin.download')); ?>?file=<?php echo e($value); ?>"><i class="fa fa-download"></i> <?php echo app('translator')->get('site.download'); ?></a>
                                                            <?php endif; ?>
                                                        </div>


                                                <?php endif; ?>


                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/admin/showemployers.css')); ?>">


    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/employers/show.blade.php ENDPATH**/ ?>