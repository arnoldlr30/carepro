<div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
    <label for="title" class="control-label"><?php echo app('translator')->get('site.title'); ?></label>
    <input required  class="form-control" name="title" type="text" id="title" value="<?php echo e(old('title',isset($note->title) ? $note->title : '')); ?>" >
    <?php echo clean( $errors->first('title', '<p class="help-block">:message</p>') ); ?>

</div>
<div class="form-group <?php echo e($errors->has('content') ? 'has-error' : ''); ?>">
    <label for="content" class="control-label"><?php echo app('translator')->get('site.content'); ?></label>
    <textarea required  class="form-control" rows="5" name="content" type="textarea" id="content" ><?php echo clean( check(old('content',isset($note->content) ? $note->content : '')) ); ?></textarea>
    <?php echo clean( $errors->first('content', '<p class="help-block">:message</p>') ); ?>

</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? __('site.update') : __('site.create')); ?>">
</div>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/notes/form.blade.php ENDPATH**/ ?>