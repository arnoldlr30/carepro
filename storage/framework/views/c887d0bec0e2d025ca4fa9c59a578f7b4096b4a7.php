<?php $__env->startSection('pageTitle',__('site.employers')); ?>
<?php $__env->startSection('page-title',__('site.employment-records').': '.$employment->employer->user->name); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div  >
                    <div  >
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_employments')): ?>
                        <a href="<?php echo e(route('admin.employments.browse')); ?>" ><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','edit_employment')): ?>
                        <a href="<?php echo e(url('/admin/employments/' . $employment->id . '/edit')); ?>" ><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.edit'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','delete_employment')): ?>
                        <form method="POST" action="<?php echo e(url('admin/employments' . '/' . $employment->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn btn-danger btn-sm" title="<?php echo app('translator')->get('site.delete'); ?>" onclick="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.delete'); ?></button>
                        </form>
                        <?php endif; ?>

                        <br/>
                        <br/>

                        <ul class="list-group">
                            <li class="list-group-item active"><?php echo app('translator')->get('Employee'); ?></li>
                            <li class="list-group-item"><a target="_blank" href="<?php echo e(route('admin.candidates.show',['candidate'=>$employment->candidate->user->id])); ?>"><?php echo e($employment->candidate->user->name); ?> (<?php echo e($employment->candidate->user->email); ?>)</a></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.start-date'); ?></li>
                            <li class="list-group-item"><?php echo e(\Illuminate\Support\Carbon::parse($employment->start_date)->format('d/M/Y')); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.end-date'); ?></li>
                            <li class="list-group-item"><?php echo e(\Illuminate\Support\Carbon::parse($employment->end_date)->format('d/M/Y')); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.active'); ?></li>
                            <li class="list-group-item"><?php echo e(boolToString($employment->active)); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.salary'); ?></li>
                            <li class="list-group-item"><?php echo e(price($employment->salary)); ?></li>

                        </ul>







                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/employments/show.blade.php ENDPATH**/ ?>