<?php $__env->startSection('pageTitle',__('site.employers')); ?>
<?php $__env->startSection('page-title',__('site.employment-records').' : '.$user->name); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div >
                    <div  >
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_employers')): ?>
                        <a href="<?php echo e(route('admin.employers.index')); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','create_employment')): ?>
                        <a href="<?php echo e(route('admin.employments.create',['user'=>$user->id])); ?>" class="btn btn-success btn-sm" title="<?php echo app('translator')->get('site.add-new'); ?>">
                            <i class="fa fa-plus" aria-hidden="true"></i> <?php echo app('translator')->get('site.add-new'); ?>
                        </a>
                        <?php endif; ?>



                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo app('translator')->get('Employee'); ?></th>
                                        <th><?php echo app('translator')->get('site.start-date'); ?></th><th><?php echo app('translator')->get('site.end-date'); ?></th>
                                        <th><?php echo app('translator')->get('site.comments'); ?></th>
                                        <th><?php echo app('translator')->get('site.active'); ?></th><th><?php echo app('translator')->get('site.actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $employments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration + ( (Request::get('page',1)-1) * $perPage)); ?></td>
                                        <td><?php echo e($item->candidate->user->name); ?></td>
                                        <td><?php echo e(\Illuminate\Support\Carbon::parse($item->start_date)->format('d/M/Y')); ?></td><td>
                                            <?php if(!empty($item->end_date)): ?>
                                            <?php echo e(\Illuminate\Support\Carbon::parse($item->end_date)->format('d/M/Y')); ?>

                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo e($item->employmentComments()->count()); ?></td>
                                        <td><?php echo e(boolToString($item->active)); ?></td>
                                        <td>
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_employment_comments')): ?>
                                            <a href="<?php echo e(route('admin.employment-comments.index',['employment'=>$item->id])); ?>" title="<?php echo app('translator')->get('site.view'); ?>"><button class="btn btn-info btn-sm"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo app('translator')->get('site.comments'); ?></button></a>
                                             <?php endif; ?>

                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ni ni-settings"></i> <?php echo app('translator')->get('site.actions'); ?>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','view_employment')): ?>
                                                    <a class="dropdown-item" href="<?php echo e(url('/admin/employments/' . $item->id)); ?>"><?php echo app('translator')->get('site.view'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','edit_employment')): ?>
                                                    <a class="dropdown-item" href="<?php echo e(url('/admin/employments/' . $item->id . '/edit')); ?>"><?php echo app('translator')->get('site.edit'); ?></a>
                                                    <?php endif; ?>

                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('access','delete_employment')): ?>
                                                    <a class="dropdown-item" href="#" onclick="$('#deleteForm<?php echo e($item->id); ?>').submit()"><?php echo app('translator')->get('site.delete'); ?></a>
                                                    <?php endif; ?>



                                                </div>
                                            </div>
                                            <form  onsubmit="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>&quot;)"  id="deleteForm<?php echo e($item->id); ?>" method="POST" action="<?php echo e(url('/admin/employments' . '/' . $item->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                             </form>







                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo clean( $employments->appends(request()->input())->render() ); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page-wide', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/employments/index.blade.php ENDPATH**/ ?>