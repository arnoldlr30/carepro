<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="title" class="control-label"><?php echo app('translator')->get('site.name'); ?></label>
    <input required  class="form-control" name="name" type="text" id="name" value="<?php echo e(old('name',isset($attachment->name) ? $attachment->name : '')); ?>" >
    <?php echo clean( $errors->first('name', '<p class="help-block">:message</p>') ); ?>

</div>

<div class="form-group <?php echo e($errors->has('file') ? 'has-error' : ''); ?>">
    <label for="file" class="control-label"><?php echo app('translator')->get('site.file'); ?></label>

    <input class="form-control"  type="file" name="file"/>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? __('site.update') : __('site.create')); ?>">
</div>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/attachments/form.blade.php ENDPATH**/ ?>