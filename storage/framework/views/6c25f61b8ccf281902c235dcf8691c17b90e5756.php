<?php $__env->startSection('pageTitle',__('site.vacancy').': '.$vacancy->title); ?>
<?php $__env->startSection('page-title',__('site.vacancy').': '.$vacancy->title); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div  >
                    <div  >

                        <a href="<?php echo e(url('/admin/vacancies')); ?>" title="<?php echo app('translator')->get('site.back'); ?>"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo app('translator')->get('site.back'); ?></button></a>
                        <a href="<?php echo e(url('/admin/vacancies/' . $vacancy->id . '/edit')); ?>" ><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.edit'); ?></button></a>

                        <form method="POST" action="<?php echo e(url('admin/vacancies' . '/' . $vacancy->id)); ?>" accept-charset="UTF-8" class="int_inlinedisp">
                            <?php echo e(method_field('DELETE')); ?>

                            <?php echo e(csrf_field()); ?>

                            <button type="submit" class="btn btn-danger btn-sm" title="<?php echo app('translator')->get('site.delete'); ?>" onclick="return confirm(&quot;<?php echo app('translator')->get('site.confirm-delete'); ?>?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo app('translator')->get('site.delete'); ?></button>
                        </form>
                        <br/>
                        <br/>

                        <ul class="list-group">
                            <li class="list-group-item active"><?php echo app('translator')->get('site.id'); ?></li>
                            <li class="list-group-item"><?php echo e($vacancy->id); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.title'); ?></li>
                            <li class="list-group-item"><?php echo e($vacancy->title); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.description'); ?></li>
                            <li class="list-group-item"><?php echo clean( check($vacancy->description) ); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.closing-date'); ?></li>
                            <li class="list-group-item"><?php echo e($vacancy->closes_at); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('site.active'); ?></li>
                            <li class="list-group-item"><?php echo e(boolToString($vacancy->active)); ?></li>
                            <li class="list-group-item active"><?php echo app('translator')->get('Total vacancies'); ?></li>
                            <li class="list-group-item"><?php echo e(($vacancy->vacancies)); ?></li>
                            <li class="list-group-item"><ul>
                                    
                                </ul></li>

                        </ul>



                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/vacancies/show.blade.php ENDPATH**/ ?>