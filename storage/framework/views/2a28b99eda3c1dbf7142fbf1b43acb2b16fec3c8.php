<?php $__env->startSection('page-title',__('site.interviews')); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('employer.interview.interview-list',['interviews'=>$interviews], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <br/>
    <?php echo e($interviews->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/interview.css')); ?>">

<?php $__env->stopSection(); ?>

<?php echo $__env->make($userLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/employer/interview/interviews.blade.php ENDPATH**/ ?>