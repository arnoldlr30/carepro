<div class="form-group <?php echo e($errors->has('user_id') ? 'has-error' : ''); ?>">
    <label for="user_id" class="control-label"><?php echo app('translator')->get('site.candidate'); ?></label>

    <select required  name="user_id" id="user_id" class="form-control">
        <?php
        $userId = old('user_id',isset($employment->candidate->user->id) ? $employment->candidate->user->id : false);
        ?>
        <?php if($userId): ?>
            <option selected value="<?php echo e($userId); ?>"><?php echo e(\App\User::find($userId)->name); ?> &lt;<?php echo e(\App\User::find($userId)->email); ?>&gt; </option>
        <?php endif; ?>
    </select>

    <?php echo clean( $errors->first('user_id', '<p class="help-block">:message</p>') ); ?>

</div>

<div class="form-group <?php echo e($errors->has('start_date') ? 'has-error' : ''); ?>">
    <label for="start_date" class="control-label"><?php echo app('translator')->get('site.start-date'); ?></label>
    <input required class="form-control date" name="start_date" type="text" id="start_date" value="<?php echo e(old('start_date',isset($employment->start_date) ? $employment->start_date : '')); ?>" >
    <?php echo clean( $errors->first('start_date', '<p class="help-block">:message</p>') ); ?>

</div>
<div class="form-group <?php echo e($errors->has('end_date') ? 'has-error' : ''); ?>">
    <label for="end_date" class="control-label"><?php echo app('translator')->get('site.end-date'); ?> (<?php echo app('translator')->get('site.optional'); ?>)</label>
    <input class="form-control date" name="end_date" type="text" id="end_date" value="<?php echo e(old('end_date',isset($employment->end_date) ? $employment->end_date : '')); ?>" >
    <?php echo clean( $errors->first('end_date', '<p class="help-block">:message</p>') ); ?>

</div>
<div class="form-group <?php echo e($errors->has('active') ? 'has-error' : ''); ?>">
    <label for="active" class="control-label"><?php echo app('translator')->get('site.active'); ?></label>
    <select  required name="active" class="form-control" id="active" >
    <?php $__currentLoopData = json_decode('{"1":"'.__('site.yes').'","0":"'.__('site.no').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('active',@$employment->active)) && old('active',@$employment->active) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
    <?php echo clean( $errors->first('active', '<p class="help-block">:message</p>') ); ?>

</div>
<div class="form-group">
    <label for="salary" class="control-label"><?php echo app('translator')->get('site.salary'); ?> (<?php echo app('translator')->get('site.optional'); ?>)</label>
    <div class="row">
        <div class="col-md-6 <?php echo e($errors->has('salary') ? 'has-error' : ''); ?>">
            <input class="form-control digit" name="salary" type="text" id="salary" value="<?php echo e(old('salary',isset($employment->salary) ? $employment->salary : '')); ?>" >
            <?php echo clean( $errors->first('salary', '<p class="help-block">:message</p>') ); ?>

        </div>
        <div class="col-md-6 <?php echo e($errors->has('salary_type') ? 'has-error' : ''); ?>">
            <select  required name="salary_type" class="form-control" id="salary_type" >
                <?php $__currentLoopData = json_decode('{"m":"'.__('site.per-month').'","a":"'.__('site.per-annum').'"}', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($optionKey); ?>" <?php echo e(((null !== old('salary_type',@$employment->salary_type)) && old('salary_type',@$employment->salary_type) == $optionKey) ? 'selected' : ''); ?>><?php echo e($optionValue); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <?php echo clean( $errors->first('salary_type', '<p class="help-block">:message</p>') ); ?>

        </div>
    </div>

</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? __('site.update') : __('site.create')); ?>">
</div>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/employments/form.blade.php ENDPATH**/ ?>