<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo app('translator')->get('site.invoice'); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/admin/invoice.css')); ?>">
</head>

<body>
<div >
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr>
            <td width="73%" align="center" valign="top" class="centext">
                <div  >
                    <?php if(!empty(setting('image_logo'))): ?>
                        <img src="<?php echo e(asset(setting('image_logo'))); ?>" class="imgdims" />
                        <?php endif; ?>
                </div>
                <div class="coyname" ><?php echo e(setting('general_site_name')); ?></div>

            </td>
            <td width="27%" valign="top"><p><?php echo clean( nl2br(clean(setting('general_address'))) ); ?>

                </p>
                <p><?php echo app('translator')->get('site.telephone-o'); ?>: <?php echo e(setting('general_tel')); ?><br />
                    <?php echo app('translator')->get('site.e-mail'); ?>: <?php echo e(setting('general_contact_email')); ?> <br />
                    <br />
                </p></td>
        </tr>
    </table>
</div>
<div class="invtext"><?php echo app('translator')->get('site.invoice'); ?></div>
<div class="mb40" ><?php echo e(\Illuminate\Support\Carbon::parse($invoice->due_date)->format('M d, Y')); ?></div>

<div><strong><?php echo app('translator')->get('site.bill-to'); ?>: <?php echo e($invoice->user->name); ?></strong></div>
<div><?php echo e($invoice->user->email); ?></div>
<div><?php echo app('translator')->get('site.invoice-number'); ?>: #<?php echo e($invoice->id); ?></div>
<?php if(!empty($invoice->due_date)): ?>
<div><?php echo app('translator')->get('site.due-date'); ?>: <?php echo e(\Illuminate\Support\Carbon::parse($invoice->due_date)->format('d/M/Y')); ?></div>
<?php endif; ?>
<div class="mb40"><?php echo app('translator')->get('site.status'); ?>: <?php if($invoice->paid==1): ?><?php echo app('translator')->get('site.paid'); ?><?php else: ?> <?php echo app('translator')->get('site.unpaid'); ?><?php endif; ?></div>

<div class="bdrtable">
    <table width="100%" border="1" cellspacing="2" cellpadding="2" class="tblbdr">
        <tr>
            <th scope="col"><?php echo app('translator')->get('site.item'); ?></th>
            <th scope="col"><?php echo app('translator')->get('site.unit-price'); ?></th>
            <th scope="col"><?php echo app('translator')->get('site.total'); ?></th>
        </tr>
        <tr>
            <td height="100" align="center" valign="top"><?php echo e($invoice->title); ?></td>
            <td align="center" valign="top"><?php echo e(price($invoice->amount)); ?></td>
            <td align="center" valign="top"><?php echo e(price($invoice->amount)); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right"><?php echo app('translator')->get('site.total-due'); ?>:</td>
            <td align="center"><strong><?php echo e(price($invoice->amount)); ?></strong></td>
        </tr>
    </table>

</div>

<div class="mtmb"><strong><?php echo app('translator')->get('site.amount-in-words'); ?>:
        <?php echo e(ucwords(convert_number_to_words($invoice->amount))); ?> <?php echo e(ucfirst(setting('general_currency_name'))); ?> <?php echo app('translator')->get('site.only'); ?></strong></div>

<div>
    <h3><?php echo app('translator')->get('site.description'); ?></h3>
   <?php echo e($invoice->description); ?>

</div>


</body>
</html>
<?php /**PATH C:\Users\Arnold Lainez\Documents\Care pro\Resources\views/admin/pdf/invoice.blade.php ENDPATH**/ ?>